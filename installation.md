# WS-2022-2023: Unsupervised deep learning with applications in image analysis

- [Exercises](https://git.hhu.de/2022-ws-2023-unsuperviseddeeplearning/exercises)

## Links and material

- sciebo folder: https://fz-juelich.sciebo.de/
- rocket chat: https://rocketchat.hhu.de/
- ILIAS: [link](https://ilias.hhu.de/ilias.php?ref_id=1318332&cmdClass=ilrepositorygui&cmdNode=y1&baseClass=ilrepositorygui)
- datalad slides: [link](https://fz-juelich.sciebo.de/s/6meCQRzFTa0hapd?path=%2Fexercises#pdfviewer)
- [How to Use t-SNE Effectively (t-SNE)](https://distill.pub/2016/misread-tsne/)

## FAQ

### All subfolders are empty. What do I do?

Subfolders are submodules, which need to be cloned explicitly.
Either clone them directly:

```bash
git clone --recursive https://git.hhu.de/2022-ws-2023-unsuperviseddeeplearning/index.git 2022-ws-2023-unsuperviseddeeplearning
```

Or after cloning the index repository:

```bash
git submodule update --init
```
