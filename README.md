# Exercise 1: Clustering and dimensionality reduction

**Submission -> November 7, 12:30 pm**

## Learning goals

- Familiarize with Python, Numpy, SciPy, scikit-learn, PyTorch.
- Download, prepare, and explore a real world dataset for analysis.
- Apply Principal Component Analysis (PCA) to the data. Inspect and understand the components.
- Apply clustering algorithms to discover structure in the data.
- Learn how dimensionality reduction benefits classification.

## Exercises

### Data preparation and exploration

- Download the `Olivetti` faces dataset from https://ndownloader.figshare.com/files/5976027. The `Olivetti` faces dataset contains images of 40 classes (persons), 10 images per class, which are continuously stored, e.g. `data[0:10]` belong to class `0`, `data[10:20]` belong to class `1`, and so on.
- Explore the downloaded data. Visualize some images, inspect their shape, data type, and the range of values.


__Tip 1:__ Use `scipy.io.matlab.loadmat` to load the data.

__Tip 2:__ If you have difficulties downloading the data, you can use [scikit-learn](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_olivetti_faces.html?highlight=olivetti#sklearn.datasets.fetch_olivetti_faces) to fetch the data.

__Advanced:__ Repeat the analysis using the `Labeled Faces in the Wild` dataset ([link](http://vis-www.cs.umass.edu/lfw/)). This dataset is also available in [scikit-learn](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_lfw_people.html).

### Principal Component Analysis (PCA)

__Note:__ Implement PCA yourself, using only `numpy`.

- Plot the eigenvalues and compute the cumulative explained variance.
- Reshape some eigenvectors (e.g., those with the highest eigenvalues) into images and visualize them.
- Reconstruct images using the first `K` eigenvectors. Visualize the results.

### Clustering

__Note:__ You may use the t-SNE implementation of scikit-learn for the following experiments.

- Apply one (or several) of the discussed clustering algorithms to the raw images, as well as to the decomposed data points. Experiment with different number of clusters.
- After clustering, apply t-SNE to reduce the dimensionality of the data to two (or three) and visualize the data points. Initilaize t-SNE randomly, and with PCA. Do you see a difference?
- Visualize examples of images belonging to each identified cluster.

## Classification

__Note:__ Use the Olivetti faces dataset for the following task.

- Split the data into a training and a test set and run a Principal Component Analysis on the training set. Find a splitting strategy that is appropriate for the given dataset.
- Use PyTorch to train a linear classifier to predict the class (person) for each image. Train the classifier using raw image pixels and PCA projections.
- Evaluate the accuracy of the trained model on the test set.
- Use the weights of the linear classifier to determine the most important features for the classification of each class.
