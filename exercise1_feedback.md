# Exercise 1 - jufar100_togla104_lueic105

## General

- If you are submitting multiple solutions per person (which is okay), make sure the naming is consistent across the files. Every file should correspond to the acronym of the author.

## jufar100

[...]

## main (togla104)

[...]
## olivetti.ipynb (lucei105)

### Comments

- Overall good solution.
- Good description of the displayed results.
- Good splitting strategy for the dataset.

### Suggestions

- Make sure to set the random seed, also for PyTorch. This makes sure we can reproduce your results. See https://pytorch.org/docs/stable/notes/randomness.html
- I would recommend using a PyTorch optimizer, although implementing gradient yourself is a good exercise.
